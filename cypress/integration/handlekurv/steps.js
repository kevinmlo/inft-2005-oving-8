import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

Given(/^at jeg har åpnet nettkiosken$/, () => {
  cy.visit("http://localhost:8080");
});

When(/^jeg legger inn varer og kvanta$/, () => {
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("4");
  cy.get("#saveItem").click();

  cy.get("#product").select("Smørbukk");
  cy.get("#quantity").clear().type("5");
  cy.get("#saveItem").click();

  cy.get("#product").select("Stratos");
  cy.get("#quantity").clear().type("1");
  cy.get("#saveItem").click();

  cy.get("#product").select("Hobby");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

Then(/^skal handlekurven inneholde det jeg har lagt inn$/, () => {
  cy.contains("Hubba bubba");
  cy.contains("Smørbukk");
  cy.contains("Stratos");
  cy.contains("Hobby");
});

And(/^den skal ha riktig totalpris$/, function () {
  cy.get("#price").should("have.text", "33");
});

And(/^lagt inn varer og kvanta$/, () => {
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("4");
  cy.get("#saveItem").click();

  cy.get("#product").select("Smørbukk");
  cy.get("#quantity").clear().type("5");
  cy.get("#saveItem").click();

  cy.get("#product").select("Stratos");
  cy.get("#quantity").clear().type("1");
  cy.get("#saveItem").click();

  cy.get("#product").select("Hobby");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

When(/^jeg sletter varer/, () => {
  cy.get("#product").select("Smørbukk");
  cy.get("#deleteItem").click();
});

Then(/^skal ikke handlekurven inneholde det jeg har slettet/, () => {
  cy.get("#list").should("not.contain.text", "Smørbukk");
});

When(/^jeg oppdaterer kvanta for en vare$/, () => {
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("1");
  cy.get("#saveItem").click();
});

Then(/^skal handlekurven inneholde riktig kvanta for varen$/, () => {
  cy.get("#list").should("contain.text", "1 Hubba bubba");
});

Given(/^at jeg har lagt inn varer i handlekurven/, () => {
  cy.get("#product").select("Hubba bubba");
  cy.get("#quantity").clear().type("4");
  cy.get("#saveItem").click();

  cy.get("#product").select("Smørbukk");
  cy.get("#quantity").clear().type("5");
  cy.get("#saveItem").click();

  cy.get("#product").select("Stratos");
  cy.get("#quantity").clear().type("1");
  cy.get("#saveItem").click();

  cy.get("#product").select("Hobby");
  cy.get("#quantity").clear().type("2");
  cy.get("#saveItem").click();
});

And(/^trykket på Gå til betaling/, () => {
  cy.get("#goToPayment").click();
});

When(
  /^jeg legger inn navn, adresse, postnummer, poststed og kortnummer$/,
  () => {
    cy.get("#fullName").type("Ola Nordmann");
    cy.get("#address").type("Postboks 1");
    cy.get("#postCode").type("0010");
    cy.get("#city").type("Oslo");
    cy.get("#creditCardNo").type("1234456776544321");
  }
);

And(/^trykker på Fullfør kjøp$/, () => {
  cy.get("#submitID").click();
  //la inn verdi på knappen for å finne med cy.get
});

Then(/^skal jeg få beskjed om at kjøpet er registrert/, () => {
  cy.get("#completed");
  //la inn verdi på knappen for å finne med cy.get
});

When(/^jeg legger inn ugyldige verdier i feltene$/, () => {
  cy.get("#fullName").clear().blur();
  //Address error
  cy.get("#address").clear().blur();
  //Postcode error
  cy.get("#postCode").clear().blur();
  //City error
  cy.get("#city").clear().blur();
  //CreditCardNo error
  cy.get("#creditCardNo").clear().blur();
});

Then(/^skal jeg få feilmeldinger for disse$/, () => {
  cy.get("#fullNameError").should("exist");
  cy.get("#addressError").should("exist");
  cy.get("#postCodeError").should("exist");
  cy.get("#cityError").should("exist");
  cy.get("#creditCardNoError").should("exist");
});
